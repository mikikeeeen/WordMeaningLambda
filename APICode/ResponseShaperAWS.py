# ■wikimediaからのレスポンスを可読性のある文章に整形するコード
# 1. リクエストしたい単語を引数に指定
# 2. リクエストをする
# 3. 整形する(printをする)
import sys
from bs4 import BeautifulSoup
from urllib import request
import urllib.parse  # URLエンコード用ライブラリ
import xml.etree.ElementTree as ET
import re
import warnings

def GetResponse(word):
    url = 'https://ja.wikipedia.org/w/api.php?format=xml&action=query&prop=revisions&titles={}&rvprop=content'
    # URLエンコードされた検索語
    wordencoded = urllib.parse.quote(word)
    # URLエンコードをした検索語をURLの中に組み込む
    url = url.format(wordencoded)
    # print(url)
    response = request.urlopen(url)
    soup = BeautifulSoup(response)
    # 流石にrevタグは複数ヒットしないだろうという予想のもと...
    # また、意味が見つからなかった場合に備えて
    try:
        meaning = soup.find('rev').contents[0]
    except AttributeError as e:
        meaning = '【エラー】該当の単語の意味が見つかりませんでした'
    return meaning


def sharping(meaning):
    # 整形の結果 もしちゃんと整形できたらOKを, 整形できない(REDIRECTを返されるなど)場合はNGを返す
    result = ''
    returntext = ''
    # 漢字の正規表現を含む(https://note.nkmk.me/python-re-regex-character-type/)
    sectionexp = r'==\s({Script=Han}+)+・*({Script=Han}+)*\s=='
    htmltagexp = r'<("[^"]*"\|\'[^\']*\'|[^\'">])*>'
    # # 太文字表現(カタカナ)
    # boldkatakanaexp = r'\'\'\'[\u30A1-\u30FF]+\'\'\''
    # # 太文字表現(ひらがな)
    # boldhiraganaexp = r'\'\'\'[\u3041-\u309F]+\'\'\''
    # # 太文字表現(漢字)
    # boldkanjiexp = r'\'\'\'(\p{Script=Han}+)+・*(\p{Script=Han}+)*\'\'\''
    # =*=*=*=*=*=*整形=*=*=*=*=*=*
    # ひとまず改行を全て無くす
    meaning = str(meaning)
    meaning = meaning.replace('\r', '').replace('\n', '').replace('\r\n', '')
    #「 == 概要 ==」みたいのを消す
    meaning = re.sub(sectionexp, '', meaning)
    # htmlタグが含まれているので削除する
    meaning = re.sub(htmltagexp, '', meaning)
    # シングルクオーテーションを削除
    meaning = meaning.replace('\'', '')
    # # 太文字表現(カタカナ)
    # meaning = regex.sub(boldkatakanaexp, '', meaning)
    # # 太文字表現(ひらがな)
    # meaning = regex.sub(boldhiraganaexp, '', meaning)
    # # 太文字表現(漢字)
    # meaning = regex.sub(boldkanjiexp, '', meaning)

    # =*=*=*=*=*=*整形終了=*=*=*=*=*=*
    lines = meaning.split('。')
    # 最初の３行分のみ返却する(多分最初の３行でなんとく意味わかるでしょ、という目論見のもとw)
    try:
        returntext = lines[0] + '\n' + lines[1] + '\n' + lines[2]
        result = 'OK'
    except IndexError as e:
        returntext = lines[0]
        result = 'NG'
    # returntext = lines[0]
    return returntext, result

def reshaping(word):
    if '#REDIRECT' in word:
        word = word.replace('#REDIRECT', '').replace(' ', '').replace('[', '').replace(']', '')
        meaning = GetResponse(word)
        sharpingoutput = sharping(meaning)
        shapedwordmeaning = sharpingoutput[0]
        shapingresult = sharpingoutput[1]   # こいつどうしようかな...
        return shapedwordmeaning
    else:
        return '【エラー】検索された単語の意味が見つかりません'
        
def ResponseShaperMain(event):
    warnings.simplefilter('ignore')
    shapedwordmeaning = ''
    word = event['word']
    meaning = GetResponse(word)
    sharpingoutput = sharping(meaning)
    shapedwordmeaning = sharpingoutput[0]
    shapingresult = sharpingoutput[1]
    # 整形に失敗したら再度実行
    if shapingresult == 'NG':
        shapedwordmeaning = reshaping(shapedwordmeaning)
    return shapedwordmeaning


# ==========上記のコードはLambdaにデプロイされている最新版のコードを編集したもの==========
if __name__ == '__main__':

    # word = '歯に衣着せぬ'
    # word = 'シニカル'
    # word = 'サパー'
    # word = 'カルト'
    # word = 'アバンギャルド'
    # word = 'アノニマス'
    # word = 'スペルマ'
    # word = 'シンポジウム'
    # word = '四阿'
    # word = 'シンギュラリティ'
    # word = 'バタフライエフェクト'
    # word = 'イデオロギー'
    # word = '鉛筆なめなめ'
    word = '恨み骨髄に徹す'
    # word = '眉唾'
    # word = '肉薄'

    event = {'word': word}
    shapedwordmeaning = ResponseShaperMain(event)
    print(shapedwordmeaning)

